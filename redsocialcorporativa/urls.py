from django.conf import settings
from django.conf.urls import patterns, include, url, static

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import TemplateView

admin.autodiscover()
from django.contrib.auth.views import login

urlpatterns = patterns('',
                       url(r'^login/$', login,
                           {"template_name": "login.html"}, name='login'),
                       url(r'^$', ensure_csrf_cookie(TemplateView.as_view(template_name="index.html")), name='index'),
                       # Examples:
                       # url(r'^$', 'redsocialcorporativa.views.home', name='home'),
                       url(r'^api/', include('website.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns = urlpatterns + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
