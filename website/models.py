from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields.files import ImageFieldFile, FileField


class Profile(models.Model):
    user = models.OneToOneField(User)
    photo = models.ImageField(upload_to="users/photos/", null=True, blank=True)
    # Info
    bio = models.TextField(null=True, blank=True)
    sex = models.BooleanField()
    job_title = models.CharField(max_length=50, null=True, blank=True)
    department = models.CharField(max_length=40, null=True, blank=True)
    location = models.CharField(max_length=100, null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)
    significant_other = models.CharField(max_length=100, null=True, blank=True)
    expertise = models.TextField(null=True, blank=True)
    interests = models.TextField(null=True, blank=True)
    # Contact
    work_phone = models.CharField(max_length=10, null=True, blank=True)
    mobile_phone = models.CharField(max_length=10, null=True, blank=True)
    im = models.CharField(max_length=20, null=True, blank=True)
    twitter_username = models.CharField(max_length=30, null=True, blank=True)
    skype_name = models.CharField(max_length=30, null=True, blank=True)
    facebook_profile = models.CharField(max_length=50, null=True, blank=True)
    linkedin_profile = models.CharField(max_length=50, null=True, blank=True)
    other_websites = models.TextField(null=True, blank=True)

    def get_thumb(self):
        from sorl.thumbnail import get_thumbnail

        im = get_thumbnail(self.photo, '50x50', crop='top') if self.photo else None
        return ImageFieldFile(instance=im, field=FileField(), name=im.name if im else None).url

    def get_photo(self):
        return self.photo.url if self.photo else None

    def get_sex(self):
        return "male" if self.sex else "female"

    def __unicode__(self):
        return self.user.username


class Circle(models.Model):
    user = models.ForeignKey(User, related_name="circles")
    name = models.CharField(max_length=20)
    people = models.ManyToManyField(User, null=True, blank=True, related_name="circled")

    def __unicode__(self):
        return self.name


class WorkGroup(models.Model):
    user = models.ForeignKey(User, related_name="workgroups_created")
    public = models.BooleanField()
    name = models.CharField(max_length=200)
    description = models.TextField()
    people = models.ManyToManyField(User, null=True, blank=True, related_name="workgroups_joined")
    admins = models.ManyToManyField(User, null=True, blank=True, related_name="workgroups_admin")

    def __unicode__(self):
        return self.name


class Work(models.Model):
    user = models.ForeignKey(User)
    employer = models.CharField(max_length=100)
    title = models.CharField(max_length=50)
    description = models.TextField()
    start_year = models.CharField(max_length=4)
    end_year = models.CharField(max_length=4)

    def __unicode__(self):
        return self.title + " - " + self.employer


class Education(models.Model):
    user = models.ForeignKey(User)
    school = models.CharField(max_length=100)
    degree = models.CharField(max_length=100)
    description = models.TextField()
    start_year = models.CharField(max_length=4)
    end_year = models.CharField(max_length=4)

    def __unicode__(self):
        return self.degree + " - " + self.school


class Topic(models.Model):
    name = models.CharField(max_length=20, db_index=True)

    def __unicode__(self):
        return self.name


class Like(models.Model):
    user = models.ForeignKey(User, related_name="my_likes")
    date_like = models.DateTimeField(auto_now_add=True)
    publication = models.ForeignKey("Publication", null=True, blank=True, related_name="likes")
    comment = models.ForeignKey("Comment", null=True, blank=True, related_name="likes")


class Publication(models.Model):
    user = models.ForeignKey(User, related_name="pubs_created")
    people_notified = models.ManyToManyField(User, null=True, blank=True, related_name="pubs_mentioned")
    text = models.TextField()
    topics = models.ManyToManyField(Topic, null=True, blank=True)
    file = models.FileField(upload_to="users/files_upload/", null=True, blank=True)
    circles = models.ManyToManyField(Circle, null=True, blank=True)
    workgroup = models.ForeignKey(WorkGroup, null=True, blank=True)
    date_pub = models.DateTimeField(auto_now_add=True)

    def total_likes(self):
        return self.likes.count()

    def __unicode__(self):
        return self.text[:10]


class Comment(models.Model):
    user = models.ForeignKey(User)
    pub = models.ForeignKey(Publication)
    texto = models.TextField()
    date_comment = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    from_user = models.ForeignKey(User, related_name="messages_received")
    to_user = models.ForeignKey(User, related_name="messages_sent")
    unread = models.BooleanField(default=True)
    text = models.TextField()
    date_sent = models.DateTimeField(auto_now_add=True)
    date_read = models.DateTimeField(null=True, blank=True)