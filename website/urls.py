from django.conf.urls import patterns, include, url, static
from website.views import PostListCreate, MeGet

urlpatterns = patterns('',
                       url(r'^me/stream/$', PostListCreate.as_view(), name='me-stream'),
                       url(r'^me/$', MeGet.as_view(), name='me'),
)