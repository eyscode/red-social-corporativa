from django.contrib import admin
from website.models import Profile, Publication, Circle, WorkGroup, Like

admin.site.register(Profile)
admin.site.register(Publication)
admin.site.register(Circle)
admin.site.register(WorkGroup)
admin.site.register(Like)