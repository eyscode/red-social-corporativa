from django.db.models.query_utils import Q
from rest_framework import generics, status
from rest_framework.response import Response
from website.models import Publication, User, Circle
from website.serializers import PublicationSerializer, UserSerializer, PublicationCreateSerializer


class PostListCreate(generics.ListCreateAPIView):
    serializer_class = PublicationSerializer
    model = Publication
    paginate_by = 10

    def get_queryset(self):
        return self.model.objects.filter(
            Q(user=self.request.user) |
            Q(people_notified__in=(self.request.user.id,)) |
            (
                (
                    Q(circles__in=Circle.objects.filter(people__in=(self.request.user.id,))) |
                    # el compartio el post con un circulo donde me tiene
                    Q(circles=None)  # el post es publico
                ) &
                Q(user__in=User.objects.filter(circled__in=self.request.user.circles.all()))
                # yo siga al que publico el post, se sigue siempre y cuando el este en mis circulos
            )
        ).order_by("-date_pub")

    def create(self, request, *args, **kwargs):
        new_data = request.DATA
        new_data["user"] = request.user.id
        serializer = PublicationCreateSerializer(data=new_data, files=request.FILES)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            headers = self.get_success_headers(serializer.data)
            return Response(PublicationSerializer(self.object).data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MeGet(generics.RetrieveAPIView):
    model = User
    serializer_class = UserSerializer

    def get_object(self, queryset=None):
        return self.request.user