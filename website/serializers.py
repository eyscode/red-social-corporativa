from rest_framework import serializers
from website.models import Publication, User, Profile


class ProfileSerializer(serializers.ModelSerializer):
    sex = serializers.CharField(source="get_sex")
    photo = serializers.CharField(source="get_photo")

    class Meta:
        model = Profile
        exclude = "id", "user"


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = "id", "first_name", "last_name", "profile"


class UserMiniSerializer(serializers.ModelSerializer):
    fullname = serializers.CharField(source="get_full_name")
    photo = serializers.CharField(source="profile.get_thumb")

    class Meta:
        model = User
        fields = "photo", "fullname", "email"


class PublicationSerializer(serializers.ModelSerializer):
    user = UserMiniSerializer()
    workgroup_name = serializers.CharField(source="workgroup.name")
    workgroup_id = serializers.CharField(source="workgroup.id")
    likes = serializers.IntegerField(source="total_likes")

    class Meta:
        model = Publication
        fields = "id", "user", "text", "topics", "file", "date_pub", "workgroup_id", "workgroup_name", "likes"


class PublicationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = "id", "text", "user"