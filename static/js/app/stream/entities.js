RSC.module("StreamApp", function (StreamApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Models
    // --------------

    StreamApp.PostModel = Backbone.Model.extend();


    // Nav App Collection

    StreamApp.PostCollection = Backbone.Collection.extend({
        url: "/api/me/stream/",
        parse: function (data, opts) {
            return data.results;
        }
    })

    var PostBox = Marionette.Controller.extend({
        getAll: function () {
            var c = new StreamApp.PostCollection();
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        }
    });

    StreamApp.addInitializer(function () {
        StreamApp.repository = new PostBox();
    });

});