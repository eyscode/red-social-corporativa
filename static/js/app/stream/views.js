RSC.module("StreamApp", function (StreamApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    StreamApp.Layout = Marionette.Layout.extend({
        template: "#stream-layout-template",
        regions: {
            newPost: "#new-post-region",
            listPost: "#post-stream-region"
        }
    });

    StreamApp.NewPostView = Marionette.ItemView.extend({
        template: "#new-post-template",
        className: "post-box",
        events: {
            "focus textarea": "onFocus",
            "click #publicate_pub": "onClick"
        },
        onFocus: function () {
            this.$(".footer").slideDown();
        },
        onClick: function () {
            var data = {text: this.$("textarea").val()};
            if (data) {
                App.vent.trigger("app:stream:create-post", data);
                this.$("textarea").val("");
                this.$("textarea").trigger("keyup");
            }
        },
        onShow: function () {
            this.$("#new_post").tagmate();
            var options = [
                {label: "George Washington", value: "george"},
                {label: "Abraham Lincoln", value: "abe"},
                {label: "Andrew Jackson", value: "andy"},
                {label: "Thomas Jefferson", value: "tj"},
                {label: "Alexander Hamilton", value: "alex"},
                {label: "John F. Kennedy", value: "jfk"},
                {label: "Teddy Roosevelt", value: "teddy"},
                {label: "Franklin Delano Roosevelt", value: "fdr"}
            ];

            this.$("#new_post").tagmate({
                exprs: {
                    "@": Tagmate.NAME_TAG_EXPR,
                    "#": Tagmate.HASH_TAG_EXPR,
                    "$": Tagmate.PRICE_TAG_EXPR,
                    "£": Tagmate.PRICE_TAG_EXPR
                },
                sources: {
                    "@": function (request, response) {
                        // use convenience filter function
                        var filtered = Tagmate.filterOptions(options, request.term);
                        response(filtered);
                    }
                },
                capture_tag: function (tag) {
                    console.log("Got tag: " + tag);
                },
                replace_tag: function (tag, value) {
                    console.log("Replaced tag: " + tag + " with: " + value);
                },
                highlight_tags: true,
                highlight_class: "hilite",
                menu_class: "menu",
                menu_option_class: "option",
                menu_option_active_class: "active"
            });
            this.$("#new_post").autosize();
            this.$(".menu").css("width", this.$(".textarea-container").width());
        }
    });

    StreamApp.PostView = Marionette.ItemView.extend({
        template: "#post-template",
        className: "post-item",
        onRender: function () {
            this.$(".date").timeago();
        }
    });

    StreamApp.PostStreamView = Marionette.CollectionView.extend({
        itemView: StreamApp.PostView,
        initialize: function () {
            App.vent.on("app:stream:create-post", this.addPost, this);
        },
        appendHtml: function (collectionView, itemView, index) {
            //para que poder insertar un modelo en cualquier index de la coleccion
            var childrenContainer = collectionView.itemViewContainer ?
                collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
            var children = childrenContainer.children();
            if (children.size() - 1 <= index) {
                childrenContainer.append(itemView.el);
            } else {
                childrenContainer.children().eq(index).before(itemView.el);
            }
        },
        addPost: function (data) {
            this.collection.create(data, {wait: true, at: 0});
        },
        onClose: function () {
            App.vent.off("app:stream:create-post", this.addPost);
        }
    });
});