RSC.module("NavApp", function (NavApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Controller
    // ----------

    NavApp.Controller = Marionette.Controller.extend({
        initialize: function (opts) {
            //this.region = opts.region;
        },
        show: function () {
            //this.region.show();
        }
    });


    // Initializer
    // -----------

    NavApp.addInitializer(function (args) {
        NavApp.controller = new NavApp.Controller;
        NavApp.controller.show();
    });

    NavApp.addFinalizer(function () {
        if (NavApp.controller) {
            NavApp.controller.close();
            delete NavApp.controller;
        }
    });
});