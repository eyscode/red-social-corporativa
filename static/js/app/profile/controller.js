RSC.module("ProfileApp", {
    startWithParent: false,
    define: function (ProfileApp, App, Backbone, Marionette, $, _) {
        "use strict";

        // Controller
        // ----------

        ProfileApp.Controller = Marionette.Controller.extend({
            initialize: function (opts) {
                this.region = opts.region;
                this.repo = opts.repo;
            },
            show: function () {
                var layout = new ProfileApp.Layout();
                this.region.show(layout);
                $.when(this.repo.getInfo()).then(function (me) {
                    var view = new ProfileApp.PersonalView({model: me});
                    layout.personal.show(view);
                });
                $.when(this.repo.getWorks()).then(function (works) {
                    var view = new ProfileApp.WorkListView({collection: works});
                    layout.works.show(view);
                });
                $.when(this.repo.getEducations()).then(function (edus) {
                    var view = new ProfileApp.EducationListView({collection: edus});
                    layout.educations.show(view);
                });
                Backbone.history.navigate("#profile")
            }
        });


        ProfileApp.addInitializer(function () {
            ProfileApp.controller = new ProfileApp.Controller({repo: ProfileApp.repository, region: RSC.content});
        });

        ProfileApp.addFinalizer(function () {
            ProfileApp.controller.close();
        });

    }
});