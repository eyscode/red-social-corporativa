RSC.module("ProfileApp", {
    startWithParent: false,
    define: function (ProfileApp, App, Backbone, Marionette, $, _) {

        // Router
        // -----------

        var Router = Marionette.AppRouter.extend({
            routes: {
                "profile": "showStream"
            },
            before: function () {
                App.startSubApp("ProfileApp");
            },
            showStream: function () {
                ProfileApp.controller.show();
            }
        });

        // Initializer
        // -----------

        App.addInitializer(function () {
            var router = new Router();
        });

    }
});