RSC.module("ProfileApp", function (ProfileApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Nav App Views
    // --------------

    ProfileApp.Layout = Marionette.Layout.extend({
        template: "#profile-layout-template",
        regions: {
            personal: ".personal-region",
            works: ".work-region",
            educations: ".education-region"
        }
    });
    ProfileApp.PersonalView = Marionette.ItemView.extend({
        template: "#personal-template"
    });
    ProfileApp.WorkView = Marionette.ItemView.extend({
        template: "#work-template"
    });
    ProfileApp.EducationView = Marionette.ItemView.extend({
        template: "#education-template"
    });
    ProfileApp.WorkListView = Marionette.CollectionView.extend({

    });
    ProfileApp.EducationListView = Marionette.CollectionView.extend({

    });
});