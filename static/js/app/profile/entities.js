RSC.module("ProfileApp", function (ProfileApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // ProfileApp Models
    // --------------

    ProfileApp.UserModel = Backbone.Model.extend({
        urlRoot: "/api/me/"
    });
    ProfileApp.WorkModel = Backbone.Model.extend();
    ProfileApp.EducationModel = Backbone.Model.extend();


    // ProfileApp Collection

    ProfileApp.WorkCollection = Backbone.Collection.extend({
        url: "/api/me/works/"
    });

    ProfileApp.EducationCollection = Backbone.Collection.extend({
        url: "/api/me/educations/"
    });

    var InfoRespository = Marionette.Controller.extend({
        getInfo: function () {
            var me = new ProfileApp.UserModel({urlRoot: "/api/me/"});
            var defferred = $.Deferred();
            me.fetch({success: function (im) {
                defferred.resolve(im);
            }});
            return defferred;
        },
        getWorks: function () {
            var c = new ProfileApp.WorkCollection();
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        },
        getEducations: function () {
            var c = new ProfileApp.EducationCollection();
            var defferred = $.Deferred();
            c.fetch({success: function (all) {
                defferred.resolve(all);
            }});
            return defferred;
        }
    });

    ProfileApp.addInitializer(function () {
        ProfileApp.repository = new InfoRespository();
    });

});