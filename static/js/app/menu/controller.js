RSC.module("MenuApp", function (MenuApp, App, Backbone, Marionette, $, _) {
    "use strict";

    // Controller
    // ----------

    MenuApp.Controller = Marionette.Controller.extend({
        initialize: function (opts) {
            //this.region = opts.region;
        },
        show: function () {
            //this.region.show();
        }
    });


    // Initializer
    // -----------

    MenuApp.addInitializer(function (args) {
        MenuApp.controller = new MenuApp.Controller;
        MenuApp.controller.show();
    });

    MenuApp.addFinalizer(function () {
        if (MenuApp.controller) {
            MenuApp.controller.close();
            delete MenuApp.controller;
        }
    });
});